/**
 * This package contains the exception classes.
 */
package com.coherentlogic.coherent.data.model.core.exceptions;