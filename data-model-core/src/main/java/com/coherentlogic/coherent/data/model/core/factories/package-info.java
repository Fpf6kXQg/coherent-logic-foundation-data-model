/**
 * This package contains all the factories used in this project. This package
 * also contains a {@link Factory} interface which should be implemented by
 * factory classes.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
package com.coherentlogic.coherent.data.model.core.factories;