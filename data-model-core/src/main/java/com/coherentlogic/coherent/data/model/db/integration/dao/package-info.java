/**
 * This package contains classes that follow the data access pattern and allow
 * create, read, update, and delete operations to be performed on the domain
 * model.
 */
package com.coherentlogic.coherent.data.model.db.integration.dao;