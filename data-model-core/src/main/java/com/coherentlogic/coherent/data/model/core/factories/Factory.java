package com.coherentlogic.coherent.data.model.core.factories;

/**
 * A specification for classes that follow the factory pattern.
 *
 * @param <T> The type of object returned from the call to the
 * {@link #getInstance()} method.
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public interface Factory {

    public <T> T getInstance ();
}
