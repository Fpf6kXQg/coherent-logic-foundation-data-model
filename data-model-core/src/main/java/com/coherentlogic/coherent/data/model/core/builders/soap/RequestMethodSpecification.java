package com.coherentlogic.coherent.data.model.core.builders.soap;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <T>
 */
public interface RequestMethodSpecification<T> {

    public abstract T doGet ();
}
